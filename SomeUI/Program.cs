﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Logging;
using SamuraiApp.Data;
using SamuraiApp.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SomeUI
{
    class Program
    {
        public static SamuraiContext _context = new SamuraiContext();

        static void Main(string[] args)
        {
            _context.Database.EnsureCreated();
            _context.GetService<ILoggerFactory>().AddProvider(new MyLoggerProvider());

            //InsertNewPkFkGraph();
            //AddBattles();
            //AddManyToManyWithObjects();
            //AddSecretIdentity();
            UsingRelatedDataForFilters();

            Console.ReadLine();
        }

        private static void UsingRelatedDataForFilters()
        {
            var samurais = _context.Samurais.Where(s => s.Quotes.Any(q => q.Text.Contains("hapy"))).ToList();
            _context.Entry(samurais?.FirstOrDefault()).Collection(s => s.Quotes).Load();
             
        }

        private static void ExplicitLoad()
        {
            var samurai = _context.Samurais.FirstOrDefault();
            _context.Entry(samurai).Collection(s => s.Quotes).Load();
            _context.Entry(samurai).Reference(s => s.SecretIdentity).Load();
        }

        private static void AddSecretIdentity()
        {
            var samurai = _context.Samurais.FirstOrDefault();
            samurai.SecretIdentity = new SecretIdentity { RealName = "Motoko Kusanagi" };
            _context.SaveChanges();
        }

        private static void EagerLoadingManyToMany()
        {
            var samuraiWithBattles = _context.Samurais
                .Include(s => s.SamuraiBattles)
                .ThenInclude(sb=>sb.Battle)
                .ToList();

            foreach (var samurai in samuraiWithBattles)
            {
                Console.WriteLine(samurai.Name + " : " + samurai.SamuraiBattles.FirstOrDefault()?.Battle.Name);
            }
        }

        private static void EagerLoadWithInclude()
        {
            var samuraiWithBattles = _context.Samurais
                .Include(s => s.Quotes)
                .ToList();

            foreach (var samurai in samuraiWithBattles)
            {
                Console.WriteLine(samurai.Name + " : " + samurai.Quotes.FirstOrDefault()?.Text );
            }
        }


        private static void InsertNewPkFkGraph()
        {
            var samurai = new Samurai
            {
                Name = "Kambei Shimada",
                Quotes = new List<Quote>
                {
                    new Quote{Text="I've come to save you"}
                }
            };
            _context.Samurais.Add(samurai);
            _context.SaveChanges();
        }





        private static void AddBattles()
        {
            _context.Battles.AddRange(
                new Battle { Name="Battle of Shiroyama",StartDate = new DateTime(1877,9,24),EndDate=new DateTime(1877,9,24)},
                new Battle { Name = "Siege of Osaka", StartDate = new DateTime(1614,1,1) , EndDate = new DateTime(1877, 9, 24) },
                new Battle { Name = "Boshin War", StartDate = new DateTime(1868,1,1), EndDate = new DateTime(1877, 9, 24) }
                );
            _context.SaveChanges();
        }

        private static void AddManyToManyWithObjects()
        {
            _context = new SamuraiContext();
            var samurai = _context.Samurais.FirstOrDefault();
            var battle = _context.Battles.FirstOrDefault();

            _context.SamuraiBattles.Add(new SamuraiBattle { Samurai = samurai, Battle = battle });
            _context.SaveChanges();
        }

        //private static void RawSqlQuery()
        //{
        //    var samurais = _context.Samurais.FromSql("EXEC FIlterSamuraiByNamePart {0}","Sam").ToList();
        //    samurais.ForEach(s => Console.WriteLine(s.Name));

        //}

        //private static void SimpleSamuraiQuery()
        //{
        //    using (var context = new SamuraiContext())
        //    {
        //        var samurais = context.Samurais.ToList();

        //        foreach (var samurai in samurais)
        //        {
        //            Console.WriteLine(samurai.Name);
        //        }
        //    }

            
        //}

        //private static void InsertMultipleSamurais()
        //{
        //    var samurai = new Samurai { Name = "Julie" };
        //    var samuraiSammy = new Samurai { Name = "Sampson" };

        //    using (var context = new SamuraiContext())
        //    {
        //        context.GetService<ILoggerFactory>().AddProvider(new MyLoggerProvider());
        //        context.Samurais.AddRange(samurai, samuraiSammy);
        //        context.SaveChanges();
        //    }
        //}

        //private static void InsertSamurai()
        //{
        //    var samurai = new Samurai { Name = "Julie" };
        //    using (var context = new SamuraiContext())
        //    {
        //        context.GetService<ILoggerFactory>().AddProvider(new MyLoggerProvider());
        //        context.Samurais.Add(samurai);
        //        context.SaveChanges();
        //    }
        //}
    }
}

